package com.example.igormanka.shoppingapplication.domain.interactor;

import com.example.igormanka.shoppingapplication.data.model.ShoppingList;

import java.util.List;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface GetArchivedListInteractor {

    void execute(Callback callback);

    interface Callback{
        void success(List<ShoppingList>shoppingLists);
        void error();
    }
}
