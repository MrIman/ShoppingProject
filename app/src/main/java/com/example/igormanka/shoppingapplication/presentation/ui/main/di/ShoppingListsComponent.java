package com.example.igormanka.shoppingapplication.presentation.ui.main.di;

import com.example.igormanka.shoppingapplication.presentation.di.component.DataComponent;
import com.example.igormanka.shoppingapplication.presentation.di.scope.PerActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.fragment.ActualFragment;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.fragment.ArchivedFragment;

import dagger.Component;

/**
 * Created by igormanka on 21.03.2018.
 */

@PerActivity
@Component(dependencies = DataComponent.class, modules = ShoppingListsContractModule.class)
public interface ShoppingListsComponent {

    void inject(ActualFragment actualFragment);
    void inject(ArchivedFragment archivedFragment);
}
