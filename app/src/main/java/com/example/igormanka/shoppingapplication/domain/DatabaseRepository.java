package com.example.igormanka.shoppingapplication.domain;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.data.model.ShoppingList;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface DatabaseRepository {

    Completable createList(String name);

    Observable<List<ShoppingList>> getArchivedLists();

    Observable<List<ShoppingList>> getActualLists();

    Completable addItem(String name, int id);

    Completable deleteItem(int id);

    Observable<List<ShoppingItem>> getOneList(int foreignId);

    Completable setItemBought(int id);

    Completable setListArchived(int id);
}
