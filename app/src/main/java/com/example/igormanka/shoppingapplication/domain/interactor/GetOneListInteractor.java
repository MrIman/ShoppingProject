package com.example.igormanka.shoppingapplication.domain.interactor;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;

import java.util.List;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface GetOneListInteractor {

    void execute(int foreignId, Callback callback);

    interface Callback{
        void success(List<ShoppingItem>shoppingItemList);
        void error();
    }
}
