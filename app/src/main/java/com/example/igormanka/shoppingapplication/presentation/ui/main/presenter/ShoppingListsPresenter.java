package com.example.igormanka.shoppingapplication.presentation.ui.main.presenter;

import android.util.Log;

import com.example.igormanka.shoppingapplication.data.model.ShoppingList;
import com.example.igormanka.shoppingapplication.domain.interactor.DeleteItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetActualListsInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetArchivedListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetListArchivedInteractor;
import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.ShoppingListsContract;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by igormanka on 21.03.2018.
 */

public class ShoppingListsPresenter implements ShoppingListsContract.Presenter {

    public static final String KEY_ACTUAL = "ACTUAL";
    public static final String KEY_ARCHIVED = "ARCHIVED";
    private final GetArchivedListInteractor getArchivedListsInteractor;
    private final GetActualListsInteractor getActualListsInteractor;
    private WeakReference<ShoppingListsContract.View> view;
    private DeleteItemInteractor deleteItemInteractor;
    private SetListArchivedInteractor setListArchivedInteractor;

    @Inject
    public ShoppingListsPresenter(WeakReference<ShoppingListsContract.View> view, GetArchivedListInteractor getArchivedListsInteractor, GetActualListsInteractor getActualListsInteractor, DeleteItemInteractor deleteItemInteractor, SetListArchivedInteractor setListArchivedInteractor) {

        this.view = view;
        this.getArchivedListsInteractor = getArchivedListsInteractor;
        this.getActualListsInteractor = getActualListsInteractor;
        this.deleteItemInteractor = deleteItemInteractor;
        this.setListArchivedInteractor = setListArchivedInteractor;
    }

    @Override
    public void fetchListsData(String listType) {
        if (listType.equals(KEY_ACTUAL)) {
            fetchActualLists();
        } else {
            fetchArchivedLists();
        }
    }

    @Override
    public void archiveList(int id) {
        setListArchivedInteractor.execute(id, new SetListArchivedInteractor.Callback() {
            @Override
            public void success() {
                view.get().onArchiveListSuccess();
            }

            @Override
            public void error() {

            }
        });
    }

    @Override
    public void deleteList(int id) {
        deleteItemInteractor.execute(id, new DeleteItemInteractor.Callback() {
            @Override
            public void success() {
                view.get().onDeleteListSuccess();
            }

            @Override
            public void error() {

            }
        });
    }

    private void fetchArchivedLists() {
        getArchivedListsInteractor.execute(new GetArchivedListInteractor.Callback() {
            @Override
            public void success(List<ShoppingList> shoppingLists) {
                Log.d("DebugArchived", shoppingLists.toString());
                handleFetchedListsOnView(shoppingLists);
            }

            @Override
            public void error() {
                Log.d("DebugActual", "Error");
                handleFetchedListsErrorOnView();
            }
        });
    }

    private void fetchActualLists() {
        getActualListsInteractor.execute(new GetActualListsInteractor.Callback() {
            @Override
            public void success(List<ShoppingList> shoppingLists) {
                Log.d("DebugActual", shoppingLists.toString());
                handleFetchedListsOnView(shoppingLists);
            }

            @Override
            public void error() {
                Log.d("DebugArchived", "Error");
            }
        });
    }

    private void handleFetchedListsErrorOnView() {

    }

    private void handleFetchedListsOnView(List<ShoppingList> shoppingLists) {
        if (view.get() != null)
            view.get().onFetchListsSuccess(shoppingLists);
    }


}
