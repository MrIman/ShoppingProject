package com.example.igormanka.shoppingapplication.data.model;

import lombok.Data;

/**
 * Created by igormanka on 21.03.2018.
 */

@Data
public class ShoppingItem {

    public static final String TABLE_NAME = "shoppingitem";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_FOREIGN = "foreignkey";
    public static final String COLUMN_BOUGHT = "bought";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + " TEXT, "
            + COLUMN_FOREIGN + " INTEGER, "
            + COLUMN_BOUGHT + " INTEGER,"
            + "created_at DATETIME DEFAULT CURRENT_TIMESTAMP)";
    int id;
    int foreignId;
    String name;
    boolean bought;
}
