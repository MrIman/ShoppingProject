package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.DeleteItemInteractor;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 22.03.2018.
 */

public class DeleteItemInteractorImpl implements DeleteItemInteractor {

    private DatabaseRepository databaseRepository;

    @Inject
    public DeleteItemInteractorImpl(DatabaseRepository databaseRepository) {
        this.databaseRepository = databaseRepository;
    }

    @Override
    public void execute(int id, final Callback callback) {
        CompletableObserver observer = new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                callback.success();
            }

            @Override
            public void onError(Throwable e) {

            }
        };
        databaseRepository.deleteItem(id).subscribe(observer);
    }
}
