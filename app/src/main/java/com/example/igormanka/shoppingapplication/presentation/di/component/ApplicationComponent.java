package com.example.igormanka.shoppingapplication.presentation.di.component;

import android.content.Context;

import com.example.igormanka.shoppingapplication.presentation.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by igormanka on 20.03.2018.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Context context();
}
