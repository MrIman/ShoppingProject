package com.example.igormanka.shoppingapplication.presentation.ui.details.di;

import com.example.igormanka.shoppingapplication.presentation.di.component.DataComponent;
import com.example.igormanka.shoppingapplication.presentation.di.scope.PerActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.details.view.activity.DetailsActivity;

import dagger.Component;

/**
 * Created by igormanka on 22.03.2018.
 */

@PerActivity
@Component(dependencies = DataComponent.class, modules = DetailsContractModule.class)
public interface DetailsComponent {

    void inject(DetailsActivity detailsActivity);
}
