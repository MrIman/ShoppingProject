package com.example.igormanka.shoppingapplication.domain.interactor;

/**
 * Created by igormanka on 23.03.2018.
 */

public interface SetItemBoughtInteractor {

    void execute(int id, Callback callback);

    interface Callback {
        void success();

        void error();
    }
}
