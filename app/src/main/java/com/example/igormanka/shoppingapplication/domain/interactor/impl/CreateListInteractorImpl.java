package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.CreateListInteractor;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 21.03.2018.
 */

public class CreateListInteractorImpl implements CreateListInteractor{


    private DatabaseRepository databaseRepository;

    @Inject
    CreateListInteractorImpl(DatabaseRepository databaseRepository){

        this.databaseRepository = databaseRepository;
    }

    @Override
    public void execute(String name, final Callback callback) {
        CompletableObserver observer = new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                callback.success();
            }

            @Override
            public void onError(Throwable e) {
                callback.error();
            }
        };
        databaseRepository.createList(name).subscribe(observer);
    }
}
