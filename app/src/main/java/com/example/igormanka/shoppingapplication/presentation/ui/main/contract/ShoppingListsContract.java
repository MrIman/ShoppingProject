package com.example.igormanka.shoppingapplication.presentation.ui.main.contract;

import com.example.igormanka.shoppingapplication.data.model.ShoppingList;

import java.util.List;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface ShoppingListsContract {

    interface View{

        void onFetchListsSuccess(List<ShoppingList> shoppingLists);

        void onDeleteListSuccess();

        void onArchiveListSuccess();
    }

    interface Presenter{
        void fetchListsData(String listType);

        void archiveList(int id);

        void deleteList(int id);
    }
}
