package com.example.igormanka.shoppingapplication.data.repository;

import com.example.igormanka.shoppingapplication.data.db.service.ShoppingDbService;
import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.data.model.ShoppingList;
import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by igormanka on 21.03.2018.
 */

public class DatabaseRepositoryImpl implements DatabaseRepository {

    private ShoppingDbService shoppingDbService;

    @Inject
    public DatabaseRepositoryImpl(ShoppingDbService shoppingDbService) {

        this.shoppingDbService = shoppingDbService;
    }

    @Override
    public Completable createList(final String name) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                shoppingDbService.insertShoppingList(name);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable addItem(final String name, final int id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                shoppingDbService.addItem(name, id);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable deleteItem(final int id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                shoppingDbService.deleteItem(id);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable setItemBought(final int id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                shoppingDbService.signItemAsBought(id);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable setListArchived(final int id) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                shoppingDbService.archiveList(id);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<ShoppingItem>> getOneList(int foreignId) {
        return Observable.just(shoppingDbService.getAllShoppingItemsByForeignId(foreignId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<ShoppingList>> getActualLists() {
        return Observable.just(shoppingDbService.getActualLists())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<ShoppingList>> getArchivedLists() {
        return Observable.just(shoppingDbService.getArchivedLists())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
