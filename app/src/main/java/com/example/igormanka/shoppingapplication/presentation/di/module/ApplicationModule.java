package com.example.igormanka.shoppingapplication.presentation.di.module;

import android.content.Context;

import com.example.igormanka.shoppingapplication.presentation.ShoppingApplicaton;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 20.03.2018.
 */

@Module
public class ApplicationModule {

    private final Context context;
    public ApplicationModule(Context context) {

        this.context = context;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return context;
    }

}
