package com.example.igormanka.shoppingapplication.presentation.ui.main.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.igormanka.shoppingapplication.presentation.ui.main.view.fragment.ActualFragment;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.fragment.ArchivedFragment;

/**
 * Created by igormanka on 20.03.2018.
 */

public class ShoppingViewPagerAdapter extends FragmentStatePagerAdapter {


    private static final int PAGES_COUNT = 2;

    public ShoppingViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0){
            ActualFragment actualFragment = new ActualFragment();
            return actualFragment;
        }
        else
            return new ArchivedFragment();
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof ArchivedFragment) {
            ((ArchivedFragment) object).update();
        }
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return PAGES_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title;
        if (position==0){
            title = "ACTUAL";
        }
        else{
            title = "ARCHIVED";
        }
        return title;
    }
}
