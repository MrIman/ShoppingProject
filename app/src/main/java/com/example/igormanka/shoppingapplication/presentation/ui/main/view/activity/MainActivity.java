package com.example.igormanka.shoppingapplication.presentation.ui.main.view.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

import com.example.igormanka.shoppingapplication.R;
import com.example.igormanka.shoppingapplication.presentation.ShoppingApplicaton;
import com.example.igormanka.shoppingapplication.presentation.ui.base.BaseActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.MainContract;
import com.example.igormanka.shoppingapplication.presentation.ui.main.di.DaggerMainComponent;
import com.example.igormanka.shoppingapplication.presentation.ui.main.di.MainContractModule;
import com.example.igormanka.shoppingapplication.presentation.ui.main.presenter.MainPresenter;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.adapter.ShoppingViewPagerAdapter;
import com.viewpagerindicator.TitlePageIndicator;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;

public class MainActivity extends BaseActivity implements MainContract.View{

    @Inject
    MainPresenter mainPresenter;
    @BindView(R.id.mainActivity_listsViewPager)
    ViewPager listsViewPager;
    @BindView(R.id.mainActivity_titlesIndicator)
    TitlePageIndicator titlePageIndicator;
    @Getter
    private ShoppingViewPagerAdapter shoppingViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpView();
    }

    @Override
    public void setUpInjection() {
        DaggerMainComponent.builder()
                .dataComponent(((ShoppingApplicaton)getApplication()).getDataComponent())
                .mainContractModule(new MainContractModule(new WeakReference<MainContract.View>(this)))
                .build()
                .inject(this);
    }

    @OnClick(R.id.mainActivity_addButton)
    void onAddListClick(){
        showAddListDialog();
    }

    public void setUpView() {
        shoppingViewPagerAdapter = new ShoppingViewPagerAdapter(getSupportFragmentManager());
        listsViewPager.setAdapter(shoppingViewPagerAdapter);
        titlePageIndicator.setViewPager(listsViewPager);
        titlePageIndicator.setTextColor(ContextCompat.getColor(this, android.R.color.darker_gray));
        titlePageIndicator.setSelectedColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    public void showAddListDialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage(R.string.addlist_message_dialog);
        alert.setView(edittext);

        alert.setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String listName = edittext.getText().toString();
                mainPresenter.addNewShoppingList(listName);
            }
        });

        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

    @Override
    public void onShopListAdded(){
        setUpView();
    }
}
