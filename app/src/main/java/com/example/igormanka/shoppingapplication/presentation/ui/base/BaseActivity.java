package com.example.igormanka.shoppingapplication.presentation.ui.base;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpInjection();
    }

    public abstract void setUpInjection();

    public void setBoundContentView(int resID){
        setContentView(resID);
        ButterKnife.bind(this);
    }
}
