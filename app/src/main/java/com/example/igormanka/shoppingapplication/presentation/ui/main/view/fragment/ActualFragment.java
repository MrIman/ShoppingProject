package com.example.igormanka.shoppingapplication.presentation.ui.main.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.igormanka.shoppingapplication.R;
import com.example.igormanka.shoppingapplication.data.model.ShoppingList;
import com.example.igormanka.shoppingapplication.presentation.ShoppingApplicaton;
import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.ShoppingListsContract;
import com.example.igormanka.shoppingapplication.presentation.ui.main.di.DaggerShoppingListsComponent;
import com.example.igormanka.shoppingapplication.presentation.ui.main.di.ShoppingListsContractModule;
import com.example.igormanka.shoppingapplication.presentation.ui.main.presenter.ShoppingListsPresenter;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.activity.MainActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.adapter.ShoppingListsAdapter;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActualFragment extends Fragment implements ShoppingListsContract.View {


    @Inject
    ShoppingListsPresenter shoppingListsPresenter;
    @BindView(R.id.list_recyclerView)
    RecyclerView shoppingListsRecyclerView;
    private List<ShoppingList> shoppingLists;

    public ActualFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_actual, container, false);
        ButterKnife.bind(this, view);
        setUpInjection();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpView();
    }

    @Override
    public void onFetchListsSuccess(List<ShoppingList> shoppingLists){
        this.shoppingLists = shoppingLists;
        shoppingListsRecyclerView.setAdapter(new ShoppingListsAdapter(getContext(), shoppingLists, new ShoppingListsAdapter.Callback() {
            @Override
            public void onArchiveClick(int id) {
                shoppingListsPresenter.archiveList(id);
                ((MainActivity) getActivity()).getShoppingViewPagerAdapter().notifyDataSetChanged();
            }
        }));
        shoppingListsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onDeleteListSuccess() {
        setUpView();
    }

    @Override
    public void onArchiveListSuccess() {
        setUpView();
    }


    private void setUpInjection() {
        DaggerShoppingListsComponent.builder()
                .dataComponent(((ShoppingApplicaton)getActivity().getApplication()).getDataComponent())
                .shoppingListsContractModule(new ShoppingListsContractModule(new WeakReference<ShoppingListsContract.View>(this)))
                .build()
                .inject(this);
    }

    private void setUpView() {
        shoppingListsPresenter.fetchListsData(ShoppingListsPresenter.KEY_ACTUAL);
    }
}
