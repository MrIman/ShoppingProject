package com.example.igormanka.shoppingapplication.presentation.ui.main.contract;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface MainContract {

    interface View{

        void onShopListAdded();
    }

    interface Presenter{

        void addNewShoppingList(String listName);
    }
}
