package com.example.igormanka.shoppingapplication.presentation.ui.details.view.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.igormanka.shoppingapplication.R;
import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by igormanka on 22.03.2018.
 */

public class ShoppingItemListAdapter extends RecyclerView.Adapter<ShoppingItemListAdapter.ViewHolder> {

    Context context;

    List<ShoppingItem> shoppingItems;
    private boolean listArchived;
    private Callback callback;

    public ShoppingItemListAdapter(Context context, List<ShoppingItem> shoppingItems, boolean listArchived, Callback callback) {

        this.context = context;
        this.shoppingItems = shoppingItems;
        this.listArchived = listArchived;
        this.callback = callback;
    }

    @Override
    public ShoppingItemListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_item_list_row, parent, false);
        return new ShoppingItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingItemListAdapter.ViewHolder holder, int position) {
        final ShoppingItem shoppingItem = shoppingItems.get(position);
        if (listArchived) {
            holder.doneButton.setVisibility(View.GONE);
            holder.deleteButton.setVisibility(View.GONE);
        }
        if (shoppingItem.isBought()) {
            holder.container.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.doneButton.setVisibility(View.GONE);
        }
        holder.nameTextView.setText(shoppingItem.getName());
        holder.doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onDoneClick(shoppingItem.getId());
            }
        });
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onDeleteClick(shoppingItem.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return shoppingItems.size();
    }

    public interface Callback {
        void onDeleteClick(int id);

        void onDoneClick(int id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.shoppingItemListRow_container)
        CardView container;

        @BindView(R.id.shoppingItemListRow_name)
        TextView nameTextView;

        @BindView(R.id.shoppingItemListRow_doneButton)
        ImageButton doneButton;

        @BindView(R.id.shoppingItemListRow_deleteButton)
        ImageButton deleteButton;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
