package com.example.igormanka.shoppingapplication.domain.interactor;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface AddItemInteractor {

    void execute(String name, int id, Callback callback);

    interface Callback{
        void success();
        void error();
    }
}
