package com.example.igormanka.shoppingapplication.presentation.ui.main.presenter;

import android.util.Log;

import com.example.igormanka.shoppingapplication.domain.interactor.CreateListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.CreateListInteractorImpl;
import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.MainContract;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by igormanka on 20.03.2018.
 */

public class MainPresenter implements MainContract.Presenter {

    private WeakReference<MainContract.View> view;
    private CreateListInteractor createListInteractor;

    @Inject
    public MainPresenter(WeakReference<MainContract.View>view, CreateListInteractor createListInteractor){

        this.view = view;
        this.createListInteractor = createListInteractor;
    }

    @Override
    public void addNewShoppingList(String listName) {
        createListInteractor.execute(listName, new CreateListInteractor.Callback() {
            @Override
            public void success() {
                view.get().onShopListAdded();
                Log.d("Debug", "Success");
            }

            @Override
            public void error() {
                Log.d("Debug", "Fail");
            }
        });
    }
}
