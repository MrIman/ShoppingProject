package com.example.igormanka.shoppingapplication.presentation.di.module;

import com.example.igormanka.shoppingapplication.data.repository.DatabaseRepositoryImpl;
import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 20.03.2018.
 */

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    DatabaseRepository provideDatabaseRepository(DatabaseRepositoryImpl databaseRepository){
        return databaseRepository;
    }
}
