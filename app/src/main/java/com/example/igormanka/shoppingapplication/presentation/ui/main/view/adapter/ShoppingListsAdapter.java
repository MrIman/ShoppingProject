package com.example.igormanka.shoppingapplication.presentation.ui.main.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.igormanka.shoppingapplication.R;
import com.example.igormanka.shoppingapplication.data.model.ShoppingList;
import com.example.igormanka.shoppingapplication.presentation.ui.details.view.activity.DetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by igormanka on 21.03.2018.
 */

public class ShoppingListsAdapter extends RecyclerView.Adapter<ShoppingListsAdapter.ViewHolder> {

    Context context;

    List<ShoppingList> shoppingLists;
    private Callback callback;

    public ShoppingListsAdapter(Context context, List<ShoppingList> shoppingLists, Callback callback) {

        this.context = context;
        this.shoppingLists = shoppingLists;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ShoppingList shoppingList = shoppingLists.get(position);
        if (shoppingList.isArchived()) {
            holder.archiveButton.setVisibility(View.GONE);
        }
        holder.nameTextView.setText(shoppingList.getName());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDetailsActivity(shoppingList.getId(), shoppingList.isArchived());
            }
        });
        holder.archiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onArchiveClick(shoppingList.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    private void startDetailsActivity(int id, boolean archived) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.SHOP_LIST_ID_KEY, id);
        intent.putExtra(DetailsActivity.LIST_ARCHIVED_KEY, archived);
        context.startActivity(intent);
        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    public interface Callback {
        void onArchiveClick(int id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.shoppingListsRow_name)
        TextView nameTextView;

        @BindView(R.id.shoppingListRow_container)
        CardView container;

        @BindView(R.id.shoppingListRow_archiveButton)
        ImageButton archiveButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
