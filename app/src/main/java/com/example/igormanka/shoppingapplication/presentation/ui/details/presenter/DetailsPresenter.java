package com.example.igormanka.shoppingapplication.presentation.ui.details.presenter;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.domain.interactor.AddItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.DeleteItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetOneListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetItemBoughtInteractor;
import com.example.igormanka.shoppingapplication.presentation.ui.details.contract.DetailsContract;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by igormanka on 22.03.2018.
 */

public class DetailsPresenter implements DetailsContract.Presenter{

    private final AddItemInteractor addItemInteractor;
    private final DeleteItemInteractor deleteItemInteractor;
    private final GetOneListInteractor getOneListInteractor;
    private WeakReference<DetailsContract.View> view;
    private SetItemBoughtInteractor setItemBoughtInteractor;

    @Inject
    public DetailsPresenter(WeakReference<DetailsContract.View> view, AddItemInteractor addItemInteractor, DeleteItemInteractor deleteItemInteractor, GetOneListInteractor getOneListInteractor, SetItemBoughtInteractor setItemBoughtInteractor) {
        this.view = view;
        this.addItemInteractor = addItemInteractor;
        this.deleteItemInteractor = deleteItemInteractor;
        this.getOneListInteractor = getOneListInteractor;
        this.setItemBoughtInteractor = setItemBoughtInteractor;
    }

    public void fetchListData(int listId) {
        getOneListInteractor.execute(listId, new GetOneListInteractor.Callback() {
            @Override
            public void success(List<ShoppingItem> shoppingItemList) {
                view.get().onFetchListSuccess(shoppingItemList);
            }

            @Override
            public void error() {

            }
        });
    }

    public void addItem(String item, int listId) {
        addItemInteractor.execute(item, listId, new AddItemInteractor.Callback() {
            @Override
            public void success() {
                view.get().onAddItemSuccess();
            }

            @Override
            public void error() {

            }
        });
    }

    @Override
    public void setItemBought(int id) {
        setItemBoughtInteractor.execute(id, new SetItemBoughtInteractor.Callback() {
            @Override
            public void success() {
                view.get().onSetItemBoughtSuccess();
            }

            @Override
            public void error() {

            }
        });
    }

    @Override
    public void deleteItem(int id) {
        deleteItemInteractor.execute(id, new DeleteItemInteractor.Callback() {
            @Override
            public void success() {
                view.get().onDeleteItemSuccess();
            }

            @Override
            public void error() {

            }
        });
    }
}
