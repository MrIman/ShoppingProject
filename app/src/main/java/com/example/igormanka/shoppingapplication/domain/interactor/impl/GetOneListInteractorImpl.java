package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.GetOneListInteractor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 21.03.2018.
 */

public class GetOneListInteractorImpl implements GetOneListInteractor {

    private DatabaseRepository databaseRepository;

    @Inject
    public GetOneListInteractorImpl(DatabaseRepository databaseRepository) {
        this.databaseRepository = databaseRepository;
    }

    @Override
    public void execute(int foreignId, final Callback callback) {
        Observer<List<ShoppingItem>>observer = new Observer<List<ShoppingItem>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<ShoppingItem> shoppingItemList) {
                callback.success(shoppingItemList);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        databaseRepository.getOneList(foreignId).subscribe(observer);
    }
}
