package com.example.igormanka.shoppingapplication.presentation.ui.main.di;

import com.example.igormanka.shoppingapplication.presentation.di.component.DataComponent;
import com.example.igormanka.shoppingapplication.presentation.di.scope.PerActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.activity.MainActivity;

import dagger.Component;

/**
 * Created by igormanka on 20.03.2018.
 */

@PerActivity
@Component(dependencies = DataComponent.class, modules = MainContractModule.class)
public interface MainComponent {

    void inject(MainActivity mainActivity);
}
