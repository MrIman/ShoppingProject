package com.example.igormanka.shoppingapplication.presentation.di.component;

import com.example.igormanka.shoppingapplication.domain.interactor.AddItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.CreateListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.DeleteItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetActualListsInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetArchivedListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetOneListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetItemBoughtInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetListArchivedInteractor;
import com.example.igormanka.shoppingapplication.presentation.di.module.ApplicationModule;
import com.example.igormanka.shoppingapplication.presentation.di.module.InteractorModule;
import com.example.igormanka.shoppingapplication.presentation.di.module.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by igormanka on 20.03.2018.
 */

@Singleton
@Component(modules = {
        InteractorModule.class,
        ApplicationModule.class,
        RepositoryModule.class
})
public interface DataComponent {

    CreateListInteractor createListInteractor();

    GetActualListsInteractor getActualListsInteractor();

    GetArchivedListInteractor getArchivedListsInteractor();

    AddItemInteractor addItemInteractor();

    DeleteItemInteractor deleteItemInteractor();

    GetOneListInteractor getOneListInteractor();

    SetItemBoughtInteractor setItemBoughtInteractor();

    SetListArchivedInteractor setItemArchivedInteractor();

}
