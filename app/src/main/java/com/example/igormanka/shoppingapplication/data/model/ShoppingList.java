package com.example.igormanka.shoppingapplication.data.model;

import lombok.Data;

/**
 * Created by igormanka on 21.03.2018.
 */

@Data
public class ShoppingList {

    public static final String TABLE_NAME = "shoppinglist";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ARCHIVED = "archived";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + " TEXT, "
            + COLUMN_ARCHIVED + " INTEGER, "
            + "created_at DATETIME DEFAULT CURRENT_TIMESTAMP)";
    //List<ShoppingItem>shoppingItems;
    boolean archived;
    int id;
    String name;

}
