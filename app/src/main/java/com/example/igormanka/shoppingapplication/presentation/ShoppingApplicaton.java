package com.example.igormanka.shoppingapplication.presentation;

import android.app.Application;

import com.example.igormanka.shoppingapplication.presentation.di.component.ApplicationComponent;
import com.example.igormanka.shoppingapplication.presentation.di.component.DaggerApplicationComponent;
import com.example.igormanka.shoppingapplication.presentation.di.component.DaggerDataComponent;
import com.example.igormanka.shoppingapplication.presentation.di.component.DataComponent;
import com.example.igormanka.shoppingapplication.presentation.di.module.ApplicationModule;
import com.example.igormanka.shoppingapplication.presentation.di.module.InteractorModule;

import lombok.Getter;

/**
 * Created by igormanka on 20.03.2018.
 */

public class ShoppingApplicaton extends Application {

    @Getter
    private ApplicationModule applicationModule;
    @Getter
    private ApplicationComponent applicationComponent;
    @Getter
    private DataComponent dataComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setUpInjection();
    }

    private void setUpInjection() {
        applicationModule = new ApplicationModule(this);

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(applicationModule)
                .build();

        dataComponent = DaggerDataComponent.builder()
                .applicationModule(applicationModule)
                .interactorModule(new InteractorModule())
                .build();
    }

}
