package com.example.igormanka.shoppingapplication.presentation.ui.details.di;

import com.example.igormanka.shoppingapplication.presentation.di.scope.PerActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.details.contract.DetailsContract;

import java.lang.ref.WeakReference;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 22.03.2018.
 */

@Module
public class DetailsContractModule {

    private WeakReference<DetailsContract.View> view;

    public DetailsContractModule(WeakReference<DetailsContract.View>view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    public WeakReference<DetailsContract.View>priovideView(){
        return this.view;
    }
}
