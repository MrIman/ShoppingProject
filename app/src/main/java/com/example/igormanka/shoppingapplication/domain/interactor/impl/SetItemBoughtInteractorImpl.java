package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.SetItemBoughtInteractor;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 23.03.2018.
 */

public class SetItemBoughtInteractorImpl implements SetItemBoughtInteractor {

    private DatabaseRepository databaseRepository;

    @Inject
    public SetItemBoughtInteractorImpl(DatabaseRepository databaseRepository) {

        this.databaseRepository = databaseRepository;
    }

    @Override
    public void execute(int id, final Callback callback) {
        CompletableObserver observer = new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                callback.success();
            }

            @Override
            public void onError(Throwable e) {

            }
        };
        databaseRepository.setItemBought(id).subscribe(observer);
    }
}
