package com.example.igormanka.shoppingapplication.presentation.ui.main.di;

import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.MainContract;
import com.example.igormanka.shoppingapplication.presentation.ui.main.view.activity.MainActivity;

import java.lang.ref.WeakReference;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 20.03.2018.
 */

@Module
public class MainContractModule {

    WeakReference<MainContract.View>view;

    public MainContractModule(WeakReference<MainContract.View> view){

        this.view = view;
    }

    @Provides
    public WeakReference<MainContract.View>providesView(){

        return view;
    }
}
