package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.data.model.ShoppingList;
import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.GetActualListsInteractor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 21.03.2018.
 */

public class GetActualListsInteractorImpl implements GetActualListsInteractor {

    private DatabaseRepository databaseRepository;

    @Inject
    public GetActualListsInteractorImpl(DatabaseRepository databaseRepository){

        this.databaseRepository = databaseRepository;
    }

    @Override
    public void execute(final Callback callback) {
        Observer<List<ShoppingList>> observer = new Observer<List<ShoppingList>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<ShoppingList> shoppingLists) {
                callback.success(shoppingLists);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        databaseRepository.getActualLists().subscribe(observer);
    }
}
