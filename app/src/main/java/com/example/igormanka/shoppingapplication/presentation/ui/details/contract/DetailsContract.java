package com.example.igormanka.shoppingapplication.presentation.ui.details.contract;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;

import java.util.List;

/**
 * Created by igormanka on 22.03.2018.
 */

public interface DetailsContract {

    interface View{

        void onFetchListSuccess(List<ShoppingItem> shoppingItemList);

        void onAddItemSuccess();

        void onSetItemBoughtSuccess();

        void onDeleteItemSuccess();
    }

    interface Presenter{

        void setItemBought(int id);

        void deleteItem(int id);
    }
}
