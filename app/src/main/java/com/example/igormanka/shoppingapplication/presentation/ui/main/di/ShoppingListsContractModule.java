package com.example.igormanka.shoppingapplication.presentation.ui.main.di;

import com.example.igormanka.shoppingapplication.presentation.di.scope.PerActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.main.contract.ShoppingListsContract;

import java.lang.ref.WeakReference;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 21.03.2018.
 */

@Module
public class ShoppingListsContractModule {

    public WeakReference<ShoppingListsContract.View>view;

    public ShoppingListsContractModule(WeakReference<ShoppingListsContract.View>view){

        this.view = view;
    }

    @Provides
    @PerActivity
    public WeakReference<ShoppingListsContract.View> provideView(){
        return this.view;
    }
}
