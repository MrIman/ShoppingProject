package com.example.igormanka.shoppingapplication.presentation.di.module;

import com.example.igormanka.shoppingapplication.domain.interactor.AddItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.CreateListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.DeleteItemInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetActualListsInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetArchivedListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.GetOneListInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetItemBoughtInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.SetListArchivedInteractor;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.AddItemInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.CreateListInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.DeleteItemInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.GetActualListsInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.GetArchivedListInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.GetOneListInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.SetItemBoughtInteractorImpl;
import com.example.igormanka.shoppingapplication.domain.interactor.impl.SetListArchivedInteractorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igormanka on 20.03.2018.
 */

@Module
public class InteractorModule {

    @Provides
    @Singleton
    public CreateListInteractor provideCreateListInteractor(CreateListInteractorImpl createListInteractor) {
        return createListInteractor;
    }

    @Provides
    @Singleton
    public GetArchivedListInteractor provideGetArchivedListInteractor(GetArchivedListInteractorImpl getArchivedListInteractor) {
        return getArchivedListInteractor;
    }

    @Provides
    @Singleton
    public GetActualListsInteractor provideGetActualListsInteractor(GetActualListsInteractorImpl getActualListsInteractor) {
        return getActualListsInteractor;
    }

    @Provides
    @Singleton
    public AddItemInteractor provideAddItemInteractor(AddItemInteractorImpl addItemInteractor) {
        return addItemInteractor;
    }

    @Provides
    @Singleton
    public DeleteItemInteractor provideDeleteItemInteractor(DeleteItemInteractorImpl deleteItemInteractor) {
        return deleteItemInteractor;
    }

    @Provides
    @Singleton
    public GetOneListInteractor provideGetOneListInteractor(GetOneListInteractorImpl getOneListInteractor) {
        return getOneListInteractor;
    }

    @Provides
    @Singleton
    public SetItemBoughtInteractor provideSetItemBoughtInteractor(SetItemBoughtInteractorImpl setItemBoughtInteractor) {
        return setItemBoughtInteractor;
    }

    @Provides
    @Singleton
    public SetListArchivedInteractor providesSetListArchivedInteractor(SetListArchivedInteractorImpl setListArchivedInteractor) {
        return setListArchivedInteractor;
    }

}
