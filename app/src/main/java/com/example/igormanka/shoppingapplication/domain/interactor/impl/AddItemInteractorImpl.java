package com.example.igormanka.shoppingapplication.domain.interactor.impl;

import com.example.igormanka.shoppingapplication.domain.DatabaseRepository;
import com.example.igormanka.shoppingapplication.domain.interactor.AddItemInteractor;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by igormanka on 22.03.2018.
 */

public class AddItemInteractorImpl implements AddItemInteractor {

    private DatabaseRepository databaseRepository;

    @Inject
    AddItemInteractorImpl(DatabaseRepository databaseRepository){

        this.databaseRepository = databaseRepository;
    }


    @Override
    public void execute(String name, int id, final Callback callback) {
        CompletableObserver observer = new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                callback.success();
            }

            @Override
            public void onError(Throwable e) {

            }
        };

        databaseRepository.addItem(name, id).subscribe(observer);
    }
}
