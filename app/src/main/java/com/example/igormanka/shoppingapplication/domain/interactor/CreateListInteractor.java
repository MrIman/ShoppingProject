package com.example.igormanka.shoppingapplication.domain.interactor;

/**
 * Created by igormanka on 21.03.2018.
 */

public interface CreateListInteractor {

    void execute(String name, CreateListInteractor.Callback callback);


    interface Callback{
        void success();
        void error();
    }

}
