package com.example.igormanka.shoppingapplication.domain.interactor;

/**
 * Created by igormanka on 22.03.2018.
 */

public interface DeleteItemInteractor {

    void execute(int id, Callback callback);

    interface Callback{
        void success();
        void error();
    }
}
