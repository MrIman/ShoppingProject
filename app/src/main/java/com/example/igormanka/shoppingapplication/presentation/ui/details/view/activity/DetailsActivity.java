package com.example.igormanka.shoppingapplication.presentation.ui.details.view.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.igormanka.shoppingapplication.R;
import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.presentation.ShoppingApplicaton;
import com.example.igormanka.shoppingapplication.presentation.ui.base.BaseActivity;
import com.example.igormanka.shoppingapplication.presentation.ui.details.contract.DetailsContract;
import com.example.igormanka.shoppingapplication.presentation.ui.details.di.DaggerDetailsComponent;
import com.example.igormanka.shoppingapplication.presentation.ui.details.di.DetailsContractModule;
import com.example.igormanka.shoppingapplication.presentation.ui.details.presenter.DetailsPresenter;
import com.example.igormanka.shoppingapplication.presentation.ui.details.view.adapter.ShoppingItemListAdapter;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailsActivity extends BaseActivity implements DetailsContract.View {

    public static final String SHOP_LIST_ID_KEY = "SHOP_LIST_ID";
    public static final String LIST_ARCHIVED_KEY = "LIST_ARCHIVED";
    @Inject
    DetailsPresenter detailsPresenter;
    @BindView(R.id.detailsActivity_recyclerView)
    RecyclerView shoppingListRecyclerView;
    @BindView(R.id.detailsActivity_addButton)
    FloatingActionButton addButton;
    private int listId;
    private boolean listArchived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBoundContentView(R.layout.activity_details);
        listId = getIntent().getExtras().getInt(SHOP_LIST_ID_KEY);
        listArchived = getIntent().getExtras().getBoolean(LIST_ARCHIVED_KEY);
        setUpView();
    }

    @Override
    public void setUpInjection() {
        DaggerDetailsComponent.builder()
                .dataComponent(((ShoppingApplicaton) getApplication()).getDataComponent())
                .detailsContractModule(new DetailsContractModule(new WeakReference<DetailsContract.View>(this)))
                .build()
                .inject(this);
    }

    @Override
    public void onFetchListSuccess(List<ShoppingItem> shoppingItemList) {
        shoppingListRecyclerView.setAdapter(new ShoppingItemListAdapter(this, shoppingItemList, listArchived, new ShoppingItemListAdapter.Callback() {
            @Override
            public void onDeleteClick(int id) {
                onDeleteItemClick(id);
            }

            @Override
            public void onDoneClick(int id) {
                onDoneItemClick(id);
            }
        }));
        shoppingListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onAddItemSuccess() {
        setUpView();
    }

    @Override
    public void onSetItemBoughtSuccess() {
        setUpView();
    }

    @Override
    public void onDeleteItemSuccess() {
        setUpView();
    }

    @OnClick(R.id.detailsActivity_addButton)
    void onAddItemClick() {
        showAddListDialog();
    }

    public void showAddListDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage("Add item");

        alert.setView(edittext);

        alert.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String item = edittext.getText().toString();
                detailsPresenter.addItem(item, listId);
            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

    private void onDoneItemClick(int id) {
        detailsPresenter.setItemBought(id);
    }

    private void onDeleteItemClick(int id) {
        detailsPresenter.deleteItem(id);
    }

    private void setUpView() {
        try {
            detailsPresenter.fetchListData(listId);
        } catch (Exception exception) {
            Toast.makeText(this, " ", Toast.LENGTH_SHORT).show();
        }
        if (listArchived) {
            addButton.setVisibility(View.GONE);
        }
    }

}
