package com.example.igormanka.shoppingapplication.data.db.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.igormanka.shoppingapplication.data.model.ShoppingItem;
import com.example.igormanka.shoppingapplication.data.model.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by igormanka on 21.03.2018.
 */

public class ShoppingDbService extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ShoppingList.db";

    @Inject
    public ShoppingDbService(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ShoppingItem.CREATE_TABLE);
        db.execSQL(ShoppingList.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + ShoppingList.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ShoppingItem.TABLE_NAME);

        onCreate(db);
    }

    public void insertShoppingList(String name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ShoppingList.COLUMN_NAME, name);
        values.put(ShoppingList.COLUMN_ARCHIVED, 0);

        db.insert(ShoppingList.TABLE_NAME, null, values);
    }

    public List<ShoppingList> getAllShoppingLists(String selectQuery) {
        List<ShoppingList> lists = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                boolean archived;
                ShoppingList shoppingList = new ShoppingList();
                shoppingList.setId(cursor.getInt(cursor.getColumnIndex(ShoppingList.COLUMN_ID)));
                shoppingList.setName(cursor.getString(cursor.getColumnIndex(ShoppingList.COLUMN_NAME)));
                archived = cursor.getInt(cursor.getColumnIndex(ShoppingList.COLUMN_ARCHIVED)) != 0;

                shoppingList.setArchived(archived);
                lists.add(shoppingList);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return lists;
    }

    public List<ShoppingItem> getAllShoppingItemsByForeignId(int foreignId) {
        String selectQuery = "SELECT * FROM " + ShoppingItem.TABLE_NAME + " WHERE " + ShoppingItem.COLUMN_FOREIGN + " = " + foreignId;

        List<ShoppingItem> list = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ShoppingItem shoppingItem = new ShoppingItem();
                shoppingItem.setId(cursor.getInt(cursor.getColumnIndex(ShoppingItem.COLUMN_ID)));
                shoppingItem.setName(cursor.getString(cursor.getColumnIndex(ShoppingItem.COLUMN_NAME)));
                shoppingItem.setForeignId(cursor.getInt(cursor.getColumnIndex(ShoppingItem.COLUMN_FOREIGN)));
                if (cursor.getInt(cursor.getColumnIndex(ShoppingItem.COLUMN_BOUGHT)) == 0) {
                    shoppingItem.setBought(false);
                } else {
                    shoppingItem.setBought(true);
                }
                list.add(shoppingItem);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return list;
    }


    public void archiveList(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ShoppingList.COLUMN_ARCHIVED, 1);
        db.update(ShoppingList.TABLE_NAME, contentValues, ShoppingList.COLUMN_ID + " = " + id, null);
    }

    public void signItemAsBought(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ShoppingItem.COLUMN_BOUGHT, 1);
        db.update(ShoppingItem.TABLE_NAME, contentValues, ShoppingItem.COLUMN_ID + " = " + id, null);

    }

    public void deleteItem(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ShoppingItem.TABLE_NAME, ShoppingItem.COLUMN_ID + "=" + id, null);
    }

    public void addItem(String name, int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ShoppingItem.COLUMN_NAME, name);
        values.put(ShoppingItem.COLUMN_FOREIGN, id);
        values.put(ShoppingItem.COLUMN_BOUGHT, 0);

        db.insert(ShoppingItem.TABLE_NAME, null, values);
    }

    public List<ShoppingList> getActualLists() {
        String selectQuery = "SELECT * FROM " + ShoppingList.TABLE_NAME + " WHERE " + ShoppingList.COLUMN_ARCHIVED + " = 0 ORDER BY created_at";

        return getAllShoppingLists(selectQuery);
    }

    public List<ShoppingList> getArchivedLists() {
        String selectQuery = "SELECT * FROM " + ShoppingList.TABLE_NAME + " WHERE " + ShoppingList.COLUMN_ARCHIVED + " = 1 ORDER BY created_at";

        return getAllShoppingLists(selectQuery);
    }
}
